import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';

class PaymentItem extends StatelessWidget {
  const PaymentItem(
      {super.key, required this.icon, required this.name, required this.value});

  final String icon;
  final String name;
  final String value;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      child: Card(
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        clipBehavior: Clip.antiAlias,
        margin: EdgeInsets.zero,
        child: ExpansionTile(
          leading: SizedBox(
            width: 80,
            child: SvgPicture.asset(
              icon,
              width: 30,
              height: 30,
              fit: BoxFit.contain,
              semanticsLabel: name,
            ),
          ),
          title: Text(name),
          children: [
            ListTile(
                title: SelectableText(value, textAlign: TextAlign.center),
                trailing: IconButton(
                  icon: const Icon(Icons.copy),
                  onPressed: () {
                    Clipboard.setData(ClipboardData(text: value));
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content: Text('Copied to clipboard'),
                      ),
                    );
                  },
                )),
          ],
        ),
      ),
    );
  }
}
