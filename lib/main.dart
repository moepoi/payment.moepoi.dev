import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:moepoi_payment/components/payment_item.dart';
import 'package:moepoi_payment/data/payment_list.dart';
import 'package:moepoi_payment/utils/utils.dart';

void main() {
  runApp(const Payment());
}

ThemeData _darkTheme = ThemeData(
    brightness: Brightness.dark,
    primaryColor: Colors.amber,
    buttonTheme: const ButtonThemeData(
      buttonColor: Colors.amber,
      disabledColor: Colors.grey,
    ));

ThemeData _lightTheme = ThemeData(
    brightness: Brightness.light,
    primaryColor: Colors.blue,
    buttonTheme: const ButtonThemeData(
      buttonColor: Colors.blue,
      disabledColor: Colors.grey,
    ));

class Payment extends StatefulWidget {
  const Payment({super.key});

  @override
  State<Payment> createState() => _PaymentState();
}

class _PaymentState extends State<Payment> {
  final _isLightTheme = false.obs;

  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  _saveThemeStatus() async {
    SharedPreferences pref = await _prefs;
    pref.setBool('theme', _isLightTheme.value);
  }

  _getThemeStatus() async {
    var isLight = _prefs.then((SharedPreferences prefs) {
      return prefs.getBool('theme') ?? false;
    }).obs;
    _isLightTheme.value = (await isLight.value);
    Get.changeThemeMode(_isLightTheme.value ? ThemeMode.light : ThemeMode.dark);
  }

  @override
  Widget build(BuildContext context) {
    _getThemeStatus();
    return GetMaterialApp(
        debugShowCheckedModeBanner: false,
        theme: _lightTheme,
        darkTheme: _darkTheme,
        themeMode: ThemeMode.system,
        home: Scaffold(
            body: Column(
          children: [
            ListTile(
              title: const Text('Payment'),
              trailing: Obx(() => Switch(
                    value: _isLightTheme.value,
                    onChanged: (value) {
                      _isLightTheme.value = value;
                      _saveThemeStatus();
                      Get.changeThemeMode(_isLightTheme.value
                          ? ThemeMode.light
                          : ThemeMode.dark);
                    },
                  )),
            ),
            Expanded(
              child: ScrollConfiguration(
                behavior: HideScrollGlow(),
                child: ListView.builder(
                  itemCount: paymentList.length,
                  itemBuilder: (context, index) {
                    return PaymentItem(
                      icon: paymentList[index]['icon'],
                      name: paymentList[index]['name'],
                      value: paymentList[index]['value'],
                    );
                  },
                ),
              ),
            ),
          ],
        )));
  }
}
