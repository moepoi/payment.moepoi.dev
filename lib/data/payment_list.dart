List<dynamic> paymentList = [
  {
    "name": 'PayPal',
    "icon": 'assets/payment/paypal.svg',
    "value": 'moepoi@protonmail.com'
  },
  {
    "name": "BCA",
    "icon": 'assets/payment/bca.svg',
    "value": '4080023206'
  },
  {
    "name": "Jago",
    "icon": 'assets/payment/jago.svg',
    "value": '108559548540'
  },
  {
    "name": "J Trust",
    "icon": 'assets/payment/jtrust.svg',
    "value": '2100267680'
  },
  {
    "name": "Mandiri",
    "icon": 'assets/payment/mandiri.svg',
    "value": '1680001624434'
  },
  {
    "name": "BTC",
    "icon": 'assets/payment/btc.svg',
    "value": 'bc1ql6v9hxfgg5g4j7mjsjr2lkh7zpzpscp0x4f0yk'
  },
  {
    "name": "ETH",
    "icon": 'assets/payment/eth.svg',
    "value": 'moepoi.eth'
  },
  {
    "name": "BNB",
    "icon": 'assets/payment/bnb.svg',
    "value": '0x6944F09dC553746a6F980c1F5A2ffbF3e68d9341'
  },
];
